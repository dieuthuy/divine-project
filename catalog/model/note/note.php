<?php
class ModelNoteNote extends Model {
    public function listNote()
    {
        $sql = "SELECT * FROM note ORDER BY date_added DESC ";
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function listNoteUpdate()
    {
        $sql = "SELECT * FROM note ORDER BY date_added DESC ";

        $query = $this->db->query($sql);

        return $query->rows;

//        $notes = $this->db->execute('SELECT note_id, created_by FROM notes')->rows;
//
//        $note_ids = array_map(function ($row) {
//            return $row['note_id'];
//        }, $notes);
//
//        $created_by_ids = array_map(function ($row) {
//            return $row['created_by'];
//        }, $notes);
//
//        $note_ids = [];
//        $created_by_ids = [];
//
//        foreach($notes as $row) {
//            $note_ids[] = $row['note_id'];
//
//            unshift($note_ids);
//            $created_by_ids = $row['created_by'];
//        }
    }

    public function addNote($data)
    {
        if ($data) {
//           return $this->db->query("INSERT INTO note SET content = '" . $this->db->escape($data) . "'");
           return $this->db->execute("INSERT INTO `note` SET content = ?s",[$data] );
        }
    }

    public function editNote( $id,$content )
    {
        if ($content && $id) {
//           return $this->db->query("UPDATE note SET content = '" . $this->db->escape($content) . "', date_modified = NOW() WHERE id = '" . (int)$id . "'");
           return $this->db->execute("UPDATE `note` SET content = ?s, date_modified = NOW() WHERE id = ?i", [$content, $id]);
        }
    }


    public function deleteNote($id)
    {
        if($id){
//            $date_add = $this->db->execute("SELECT date_added FROM `note` WHERE id = ?i\", [$id] ");
            return $this->db->execute("DELETE FROM `note` WHERE id = ?i", [$id]);
//            return  $this->db->query("DELETE FROM " . DB_PREFIX . "note WHERE id = '" . (int) $id . "'");
        }
    }

    public function changeStatus($id)
    {
        if ($id) {
//            return $this->db->query("SELECT status FROM note WHERE id = '" . (int)$id . "'")->row;
            return $this->db->execute("SELECT status FROM `note` WHERE id = ?i", [$id])->row;
        }
    }
    public function updateStatus($id, $status)
    {
//       return $this->db->query("UPDATE note SET status = '" . (int) $status . "' WHERE id = '" . (int) $id . "'");
       return $this->db->execute("UPDATE `note` SET status = ?i WHERE id = ?i", [$status, $id]);
    }
    public function undoNote($data, $date_added,$status)
    {
        if ($data && $date_added) {
            return $this->db->execute("INSERT INTO `note` SET content = ?s,date_added = ?s, status = ?i",[$data,$date_added,$status] );
        }
    }

    public function getNote($id) {

        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "note WHERE $id = '" . (int)$id . "'");

        return $query->row;
    }


    public function getNoteByDate($date_added) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "note WHERE date_added = '" . $this->db->escape(mb_strtolower($date_added)) . "'");

        return $query->row;
    }

    public function getNoteBySearch($data = [])
    {
        $data['customer_id'] = $this->note->getId();

        $sql = "SELECT
                *
            FROM
                note
            WHERE id != ?id:i
            AND IF(content IS NOT NULL AND content != '' , content LIKE ?search:%s%, date_added LIKE ?search:%s%)
            LIMIT ?start:i, ?limit:i";

        $query = $this->db->execute($sql, $data);
        return $query->rows;
    }


}