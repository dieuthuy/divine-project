<?php

class ControllerApiNote extends Controller
{
    public function index()
    {
        $this->load->model('note/note');
        $data = [];
        $notes = $this->model_note_note->listNote();
        $data['notes'] = [];
        $data['http'] = HTTP_SERVER;
        if ($notes) {
            foreach ($notes as $note) {
                $data['notes'][] = [
                    'id'         => $note['id'],
                    'content'    => $note['content'],
                    'status_raw' => $note['status'],
                    'status'     => $note['status'] == 1 ? 'Hoàn thành' : 'Chưa hoàn thành',
                    'date_added' => $note['date_added'],
                    'date_modified' => $note['date_modified'],
                ];
            }
        }
        $this->response->setOutput($this->load->view('note/note', $data));
    }

    public function list_note()
    {
        $this->load->model('note/note');
        $data = [];
        $notes = $this->model_note_note->listNoteUpdate();

        $data['notes'] = [];

        if ($notes) {
            foreach ($notes as $note) {
                $data['notes'][] = [
                    'id'         => $note['id'],
                    'content'    => $note['content'],
                    'status_raw' => $note['status'],
                    'status'     => $note['status'] == 1 ? 'Hoàn thành' : 'Chưa hoàn thành',
                    'date_added' => $note['date_added'],
                    'date_modified' => $note['date_modified'],
                ];
            }
        }

        $this->response->setOutput($this->load->view('note/note_ajax', $data));
    }

    public function add()
    {
        $this->load->model('note/note');
        $json = [];
        $json['success'] = 0;
        $content = $this->request->post['content'] ?? null;
            if ($content) {
                $this->model_note_note->addNote($content);
                $json['success'] = 1;
                $json['message'] = 'Bạn đã thêm thành công!';
            } else {
                $json['message'] = 'Không được để trống nội dung!';
            }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function edit()
    {
        $this->load->model('note/note');
        $json = [];
        $json['success'] = 0;
        $id = $this->request->post['id'] ?? 0;
        $content = $this->request->post['content'];
        if ($id && $content) {
            $this->model_note_note->editNote($id,$content);

            $json['success'] = 1;
            $json['message'] = 'Bạn đã sửa thành công!';
        } else {
            $json['message'] = 'Bạn không được để trống ID hoặc nội dung!';
        }

        $json['content'] = $content;
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function delete()
    {
        $this->load->model('note/note');
        $json = [];
        $json['success'] = 0;
        $id = $this->request->post['id'] ?? 0;
        if ($id) {
            $this->model_note_note->deleteNote($id);
            $json['success'] = 1;
            $json['message'] = 'Bạn xóa thành công!';
        } else {
            $json['message'] = 'Bạn không được để trống ID!';
        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function complete()
    {
        $this->load->model('note/note');
        $json = [];
        $json['success'] = 0;

        $id = $this->request->post['id'] ?? 0;

        if ($id) {
            $note_info = $this->model_note_note->changeStatus($id);
            if ($note_info) {
                if ($note_info['status'] == 0) {
                    $status = 1;
                } else {
                    $status  = 0;
                }
                $this->model_note_note->updateStatus($id, $status);
                $json['success'] = 1;
                $json['message'] = 'Bạn đã cập nhật trạng thái thành công!';
            }
        } else {
            $json['message'] = 'Bạn không được để trống ID!';
        }
        $json['status_raw'] = $status;
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function undo(){
        {
            $this->load->model('note/note');
            $json = [];
            $json['success'] = 0;
            $content = $this->request->post['content'] ?? null;
            $date_added = $this->request->post['date_added'] ?? null;
            $status = $this->request->post['status'] ?? null;
            if ($content) {
                $this->model_note_note->undoNote($content,$date_added,$status);
                $json['success'] = 1;
                $json['message'] = 'Bạn đã thêm thành công!';
            } else {
                $json['message'] = 'Không được để trống nội dung!';
            }
            $json['content'] = $content;
            $json['date-added'] = $date_added;
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
        }
    }

    public function autocomplete()
    {
        $data = [];
        $this->load->model('note/note');

        if (isset($this->request->get['search'])) {
            if (isset($this->request->get['search'])) {
                $search = $this->request->get['search'];
            } else {
                $search = '';
            }

            $filter_data = [
                'search' => $search,
                'start' => 0,
                'limit' => 5
            ];

            $notes = $this->model_note_note->getNoteBySearch($filter_data);

            $data['notes'] = [];

            if ($notes) {
                foreach ($notes as $note) {
                    $data['notes'][] = [
                        'id'         => $note['id'],
                        'content'    => $note['content'],
                        'status_raw' => $note['status'],
                        'status'     => $note['status'] == 1 ? 'Hoàn thành' : 'Chưa hoàn thành',
                        'date_added' => $note['date_added'],
                        'date_modified' => $note['date_modified'],
                    ];
                }
            }
        }
        $sort_order = [];

        foreach ($data as $key => $value) {
            $sort_order[$key] = $value['content'];
        }
        array_multisort($sort_order, SORT_ASC, $data);
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput($this->load->view('note/note', $data));
    }

}
