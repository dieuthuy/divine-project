<?php

trait DatabaseToolkit
{
    private $constants = [
        [
            'TRUE',
            'DB_PREFIX_',
        ],
        [
            '1 = 1',
            DB_PREFIX,
        ],
    ];

    private $isProfiling = false;

    /**
     * Build and execute a query. If profiling is enabled, profile the execution.
     */
    public function execute($query, $params = [])
    {
        if ($this->isProfiling) {
            return $this->executeProfile($query, $params);
        }

        $query = $this->build($query, $params);

        return $this->query($query);
    }

    /**
     * Build, execute and explain a query. The process exits after the execution.
     */
    public function explain($query, $params = [])
    {
        echo 'query spec:' . PHP_EOL;
        var_dump($query);

        echo 'params:' . PHP_EOL;
        var_dump($params);

        $query = $this->concatenate($query, $params);

        echo 'concatenated query spec:' . PHP_EOL;
        echo $query . PHP_EOL;

        $query = $this->build($query, $params);

        echo 'query:' . PHP_EOL;
        echo $query . PHP_EOL;

        $explain = $this->query("EXPLAIN $query")->rows;

        echo 'explain:' . PHP_EOL;
        var_dump($explain);

        $start = microtime(true);

        $result = $this->query($query)->rows;

        $end = microtime(true);
        $execution_time = ($end - $start) * 1e+3;

        echo 'execution time:' . PHP_EOL;
        echo "${execution_time}ms" . PHP_EOL;

        echo 'result:' . PHP_EOL;
        var_dump($result);

        die();
    }

    /**
     * Build a query.
     */
    public function build($query, $params = [])
    {
        $query = $this->concatenate($query, $params);
        $query = $this->evaluateConstants($query);
        $query = $this->prependPrefix($query);
        $query = $this->bindParams($query, $params);

        return $query;
    }

    public function concatenate($query, $params)
    {
        if (is_string($query)) {
            // if the query is multiline,
            // each line is a segment
            if (strpos($query, "\n") === false) {
                return $query;
            }

            $query = explode("\n", $query);
            $query = array_filter(array_map('trim', $query));
        }

        if (!is_array($query)) {
            throw new \Exception('query must be a string or an array');
        }

        $segments = [];

        foreach ($query as $segment) {
            $matches = null;

            // if param type is emtpy,
            // it's a condition
            preg_match_all('/\?(\w+):(\w?)/', $segment, $matches);

            // if the segment contains no params,
            // it won't ever be ignored
            if (empty($matches) || empty($matches[1])) {
                $segments[] = $segment;
                continue;
            }

            $is_ignored = false;

            // if there has any key that doesn't exist in the params or
            //   the param is a condition and it evaluates to false,
            // the segment is ignored
            foreach ($matches[1] as $index => $key) {
                if (
                    !array_key_exists($key, $params)
                    || ($matches[2][$index] === '' && !$params[$key])
                ) {
                    $is_ignored = true;
                    break;
                }
            }

            if ($is_ignored) {
                continue;
            }

            // remove void params
            $segments[] = preg_replace('/\s\?\w+:(\s|$)/', '$1', $segment);
        }

        return implode(' ', $segments);
    }

    /**
     * Build, execute and profile a query.
     */
    public function executeProfile($query, $params = [])
    {
        $query = $this->build($query, $params);

        echo 'query:' . PHP_EOL;
        echo $query . PHP_EOL;

        $start = microtime(true);

        $result = $this->query($query);

        $end = microtime(true);
        $execution_time = ($end - $start) * 1e+3;

        echo 'execution time:' . PHP_EOL;
        echo "${execution_time}ms" . PHP_EOL;

        echo 'stack trace:' . PHP_EOL;
        debug_print_backtrace(2);

        return $result;
    }

    /**
     * Enable or disable profiling mode, which dumps queries and execution times.
     */
    public function profile($enable = true)
    {
        $this->isProfiling = $enable;
    }

    private function bindParams($query, $params)
    {
        $index = 0;

        return preg_replace_callback(
            '/\?(\w+)?(:)?(\[)?(%)?(\w+)(%)?(\])?/',
            function ($match) use ($params, &$index) {
                $spec = $match[0];

                // throws an exception if name is specified but not proceeded by a colon
                if ($match[1] !== '' && $match[2] === '') {
                    throw new \Exception("malformed param spec: $spec");
                }

                // $name falls back to $index if not specified
                // this makes mixing integer and string keys possible but
                // it is advisable that you should avoid that
                $name = $match[1] !== '' ? $match[1] : $index++;

                if (!array_key_exists($name, $params)) {
                    throw new \Exception("param name: $name is missing: $spec");
                }

                $type = $match[5];
                // $type might be wrapped inside a [ ] to indicate that
                // an array of $type is expected
                $array_token = $match[3] . ($match[7] ?? '');

                $param = $params[$name];
                $wildcards = [$match[4], $match[6] ?? ''];

                switch ($array_token) {
                    case '':
                        if (strlen($type) === 1) {
                            return $this->escapeParam($type, $param, $wildcards);
                        }

                        $enum = $params[$type] ?? null;

                        if (!$enum || !is_array($enum)) {
                            throw new \Exception("malformed param spec: $spec");
                        }

                        return $this->escapeParam('e', [$param, $enum]);
                    case '[]':
                        if (!is_array($param)) {
                            throw new \Exception("param name: $name must be an array: $spec");
                        }

                        $param = array_map(
                            function ($param) use ($type) {
                                return $this->escapeParam($type, $param);
                            },
                            $param
                        );
                        $param = implode(',', $param);

                        return "($param)";
                    default:
                        throw new \Exception("malformed param spec: $spec");
                }
            },
            $query
        );
    }

    private function prependPrefix($query)
    {
        if (empty(DB_PREFIX)) {
            return $query;
        }

        // tables involved in the query whose name isn't prefixed
        $tables = [];
        $table_pattern = '/((?:^|\W)(?:FROM|JOIN|UPDATE|INTO)\s+(?:(?:`\w+`|`?' . DB_PREFIX . '\w+`?)\s*,\s*)*)(?!' . DB_PREFIX . ')(\w+)/i';

        do {
            $count = 0;

            // STRAIGHT_JOIN and other keywords are not supported
            // in order to use those keywords, ALTER TABLE for example, use DB_PREFIX_
            $query = preg_replace_callback(
                $table_pattern,
                function ($match) use (&$tables, &$count) {
                    return $this->prependPrefixToTable($match, $tables, $count);
                },
                $query
            );
        } while ($count > 0);

        if (!empty($tables)) {
            $tables = implode('|', array_unique($tables));

            $query = preg_replace(
                '/(' . $tables . ')(\s*\.)/',
                DB_PREFIX . '$1$2',
                $query
            );
        }

        return $query;
    }

    private function prependPrefixToTable($match, &$tables, &$count)
    {
        $replace = $match[1] . DB_PREFIX . $match[2];

        // if the replacement is the same as the matched string,
        // this loop might loop forever
        if ($replace === $match[0]) {
            throw new \Exception("possibly catastrophic pattern: $replace");
        }

        // manually increase $count is more reliable than preg replace count
        $count++;

        // to prepend prefix to table names in select expressions later
        $tables[] = $match[2];

        return $replace;
    }

    private function escapeParam($type, $param, $wildcards = null)
    {
        switch ($type) {
            case 'd':
                return (string)(float)$param;
            case 'i':
                return (string)(int)$param;
            case 'e':
                $value = $param[0];
                $enum = $param[1];

                if (in_array($value, $enum)) {
                    return $value;
                }

                foreach ($enum as $value) {
                    return $value;
                }

                throw new \Exception('invalid enum type');
            case 's':
                // avoid unecessary round trips to server
                $param = empty($param) ? '' : $this->escape($param);

                if ($wildcards !== null) {
                    $param = $wildcards[0] . $param . $wildcards[1];
                }

                return "'$param'";
            default:
                throw new \Exception("invalid param type: $type");
        }
    }

    private function evaluateConstants($query)
    {
        return str_ireplace($this->constants[0], $this->constants[1], $query);
    }
}
