<?php

class Controllercustomerpersonnel extends Controller
{
    private $error = [];

    public function index()
    {
        $this->load->language('customer/customer');

        $this->document->setTitle('heading_title');
        $this->session->data['success'] = $this->language->get('text_success');

        $this->List();
    }





    //validate
    protected function validateForm()
    {

        if ($this->error && !isset($this->error['warning'])) {
            $this->error['warning'] = 'Cảnh báo hãy kiểm tra lại các trường thông tin';
        }


        return !$this->error;
    }

    //get form
    public function getForm()
    {
        $this->load->model('customer/personnel');


        if (isset($this->request->post['gia_dinh'])) {
            $data['gia_dinh'] = $this->request->post['gia_dinh'];
        } elseif (!empty($customer_info)) {
            $data['gia_dinh'] = json_decode($customer_info['gia_dinh'], true);

            if (isset($data['gia_dinh']['quan_he'])) {
                $data['count'] = count($data['gia_dinh']['quan_he']);
            } else {
                $data['count'] = 0;
            }
        } else {
            $data['gia_dinh'] = '';
        }





        $list_permission = $this->model_customer_personnel->ListNhansuEdit();
        $PermissionDepartment   = $this->model_customer_personnel->PermissionDepartmentEdit();


        $data['tab_states'] = array_map(
            function ($has_permission) use (&$has_active_tab) {
                if ($has_active_tab) {
                    return false;
                }

                $has_active_tab = $has_permission;

                return $has_permission;
            },
            array_values($list_permission)
        );

        if (isset($list_permission['tab-general'])) {
            $data['tab_general'] = "has_permission";
        }elseif(isset($PermissionDepartment['tab-general'])){
            $data['tab_general'] = "has_permission";
        } else {
            $data['tab_general'] = "Bạn không có quyền truy cập !!";
        }


        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('customer/personnel_form', $data));
    }



    //validatedelete
    public function validateDelete()
    {
        if (!$this->user->hasPermission('modify', 'customer/personnel')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }

    //xóa nhân sự

}
