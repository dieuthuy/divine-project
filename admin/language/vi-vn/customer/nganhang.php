<?php

// Heading
$_['heading_title']             = 'Ngân hàng';

// Text
$_['text_success_add']          = 'Thành công: Bạn vừa thêm mới!';
$_['text_success_del']          = 'Thành công: Bạn vừa xóa thành công!';
$_['text_list']                 = 'Thêm Nhân Viên';
$_['text_add']                  = 'Danh sách Nhân Viên';
$_['text_success_edit']         = 'sửa ngân hàng thành công';


// Help
$_['help_safe']                 = 'Đặt đúng sự thật để tránh cho Nhân Viên này không bị hệ thống chống gian lận bắt gặp';
$_['help_affiliate']            = 'Bật / Tắt khả năng của Nhân Viên sử dụng hệ thống đại lý.';
$_['help_tracking']             = 'Mã theo dõi sẽ được sử dụng để theo dõi các giới thiệu.';
$_['help_commission']           = 'Phần trăm Đại lý nhận được trên mỗi đơn đặt hàng.';
$_['help_points']               = 'Người dùng trừ đi để xóa điểm';

// Error
$_['error_warning']             = 'Cảnh báo: Vui lòng kiểm tra cẩn thận biểu mẫu để biết lỗi!';
