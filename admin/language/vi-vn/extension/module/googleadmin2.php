<?php

// Heading
$_['doc_title']    = 'Login Admin bằng Google phía Client';
$_['heading_title']    = 'Login Admin bằng Google phía Client';

$_['text_module']      = 'Modules';
$_['text_success']     = 'Thành công: Bạn đã sửa đổi Đăng nhập Google!';
$_['text_edit']        = 'Chỉnh sửa Đăng nhập Admin với Google';

// Entry
$_['entry_status']     = 'Status';
$_['entry_clientid']     = 'Google CLIENT ID';
$_['entry_clientsec']     = 'Google CLIENT SECRET';
$_['entry_redurl']     = 'Redirect Url (Return Url)';
$_['entry_imgsrc']     = 'Hình nút Login Google';
$_['entry_appname']     = 'Tên ứng dụng';
$_['entry_devkey']     = 'Developer Key';

// Error
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi Đăng nhập bằng Google!';
