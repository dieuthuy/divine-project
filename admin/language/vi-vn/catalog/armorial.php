<?php

// Heading
$_['heading_title']          = 'Chứng Chỉ';

// Text
$_['text_success']           = 'Thành công: Chứng chỉ đã được thay đổi!';
$_['text_list']              = 'Danh sách chứng chỉ';
$_['text_add']               = 'Thêm Chứng Chỉ';
$_['text_edit']              = 'Chỉnh sửa Chứng Chỉ';
$_['text_default']           = 'Mặc định';
$_['text_keyword']           = 'Không sử dụng khoảng trống, nếu cần khoảng trống dùng - và từ Khóa phải chung cho toàn hệ thống.';

// Help
$_['help_filter']            = 'Tự động';
$_['help_top']               = 'Hiển thị lên Menu. Chỉ có tác dụng với Menu ngang cấp cao nhất';
$_['help_column']            = 'Số thứ tự cột để sử dụng cho 3 danh mục phía dưới. Chỉ có tác dụng đối với các danh mục cấp cao nhất.';

// Error
$_['error_warning']          = 'Cảnh báo: Dữ liệu bắt buộc chưa được nhập vào. Kiểm tra các ô trống!';
$_['error_permission']       = 'Cảnh báo: Bạn không được phép thay đổi các danh mục!';
$_['error_name']             = 'Tên của huy hiệu là trống. Ít nhất phải có 5 kí tự!';
$_['error_requirement_course']       = 'Bạn không được để trống các khóa học bắt buộc phải hoàn thành mới nhận được chứng chỉ này!';
