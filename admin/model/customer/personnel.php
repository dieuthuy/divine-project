<?php

class ModelCustomerPersonnel extends Model
{
    public function addPersonnel($data)
    {
        $ngay_sinh  = $data['ngay_sinh'] != '' ? date("Y-m-d", strtotime(str_replace("/", "-", $data['ngay_sinh']))) : '';
        $ngay_cap   = $data['ngay_cap'] != '' ? $this->db->escape($data['ngay_cap']) : '';

        $this->db->query("INSERT INTO " . DB_PREFIX . "note SET
		id 				= '" . (int)($data['id']) . "', 
		note 				= '" . (isset($data['note']) ? json_encode($data['note'], JSON_UNESCAPED_UNICODE) : json_encode([])) . "',
		date_added 				= NOW()");


    }

    public function edit($id, $data)
    {
        $date       = $data['ngay_sinh'] != '' ? date("Y-m-d", strtotime(str_replace("/", "-", $data['ngay_sinh']))) : '';
        $ngay_cap   = $data['ngay_cap'] != '' ? $this->db->escape($data['ngay_cap']) : '';

        $this->db->query("UPDATE " . DB_PREFIX . "note SET
		id				= '" . (int)($data['id']) . "', 
		gia_dinh 				= '" . (isset($data['gia_dinh']) ? json_encode($data['gia_dinh'], JSON_UNESCAPED_UNICODE) : json_encode([])) . "',
		date_modified 			= NOW() ");


    }

    public function getData($nhan_su_id)
    {
        $query = $this->db->execute('SELECT * FROM hrm_nhan_su WHERE nhan_su_id = ?i', [$nhan_su_id]);
        return $query->row;
    }


    public function ajax($txt)
    {
        $query = $this->db->query("SELECT path_with_type,name,id,path FROM " . DB_PREFIX . "hrm_xa_phuong where name  like '%$txt%'  or path_with_type like '$txt%' or path like '%$txt%'");


        return $query->rows;
    }


    public function deletePersonnel($nhan_su_id)
    {
        if (!empty($nhan_su_id)) {
            $data_customer = $this->db->execute('SELECT nhan_su_id,customer_id FROM hrm_nhan_su WHERE nhan_su_id = ?i', [$nhan_su_id])->row;

            $this->db->query("DELETE FROM " . DB_PREFIX . "customer 			WHERE customer_id = '" . (int)$data_customer['customer_id'] . "'");
            $this->db->query("DELETE FROM " . DB_PREFIX . "customer_activity 	WHERE customer_id = '" . (int)$data_customer['customer_id'] . "'");
            $this->db->query("DELETE FROM " . DB_PREFIX . "customer_affiliate 	WHERE customer_id = '" . (int)$data_customer['customer_id'] . "'");
            $this->db->query("DELETE FROM " . DB_PREFIX . "customer_approval 	WHERE customer_id = '" . (int)$data_customer['customer_id'] . "'");
            $this->db->query("DELETE FROM " . DB_PREFIX . "customer_reward 		WHERE customer_id = '" . (int)$data_customer['customer_id'] . "'");
            $this->db->query("DELETE FROM " . DB_PREFIX . "customer_transaction WHERE customer_id = '" . (int)$data_customer['customer_id'] . "'");
            $this->db->query("DELETE FROM " . DB_PREFIX . "customer_ip 			WHERE customer_id = '" . (int)$data_customer['customer_id'] . "'");
            $this->db->query("DELETE FROM " . DB_PREFIX . "address 				WHERE customer_id = '" . (int)$data_customer['customer_id'] . "'");
        }

        $this->db->execute("DELETE FROM  hrm_nhan_su WHERE nhan_su_id = ?i", [$nhan_su_id]);
        $this->db->execute("DELETE FROM  hrm_cong_viec WHERE nhan_su_id = ?i", [$nhan_su_id]);
        $this->db->execute("DELETE FROM  hrm_bao_hiem WHERE nhan_su_id = ?i", [$nhan_su_id]);
        $this->db->execute("DELETE FROM  hrm_tiep_nhan WHERE nhan_su_id = ?i", [$nhan_su_id]);
        $this->db->execute("DELETE FROM  hrm_hop_dong WHERE nhan_su_id = ?i", [$nhan_su_id]);
        $this->db->execute("DELETE FROM  dangky_khoahoc WHERE user_id = ?i", [$nhan_su_id]);
    }

    public function getPersonnel($data = [], $filter_types = [])
    {
        $sql = ("SELECT *  FROM hrm_nhan_su AS ns  JOIN hrm_cong_viec AS cv ON (ns.nhan_su_id = cv.nhan_su_id) ");
        $implode = [];
        if (!empty($data['filter_name'])) {
            $implode[] = " ns.ho_ten LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (!empty($data['filter_ma'])) {
            $implode[] = " ns.ma_nhan_su = '" . (int)($data['filter_ma']) . "'";
        }

        if (!empty($data['filter_email'])) {
            $implode[] = " ns.email LIKE '%" . $this->db->escape($data['filter_email']) . "%'";
        }

        if (!empty($data['filter_email_cty'])) {
            $implode[] = " ns.email_cty LIKE '%" . $this->db->escape($data['filter_email_cty']) . "%' ";
        }

        if ($implode) {
            $sql .= " AND " . implode(" AND ", $implode);
        }


        if ($filter_types) {
            foreach ($filter_types as $filter_type) {
                if (trim($filter_type['value']) != '') {
                    $sql .= $filter_type['query'];
                }
            }
        } else {
            $sql .= " WHERE cv.status = 0 ";
        }

        if (isset($data['filter_sort']) && ($data['filter_order'] == 'DESC')) {
            $sql .= " ORDER BY " . $data['filter_sort'];
        } else {
            $sql .= " ORDER BY ma_nhan_su ";
        }
        if (isset($data['filter_order']) && $data['filter_order'] == 'DESC') {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }


        $query = $this->db->execute($sql);

        return $query->rows;
    }



    public function getUser()
    {
        $query = $this->db->execute('SELECT user_id,username,email   FROM user ORDER BY username DESC');

        return $query->rows;
    }


}
